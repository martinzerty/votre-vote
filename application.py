from flask import Flask, render_template, flash, redirect, request, make_response, jsonify, session, send_from_directory
import datalogger
from datetime import timedelta
import os

app = Flask(__name__)
app.secret_key = b"_i5-H0-OFXLHgDU7MV0P5oxfk7fm1EAlmoVZIIwByJc=="
app.permanent_session_lifetime = timedelta(minutes=40)
log = datalogger.Logger()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/session/")
@app.route("/session/<token>")
def acces_sess(token=""):
    if len(token) == 30:
        result = log.users_in_sess(token)
        if len(result) > 0:
            name = log.get_sess_name(token)[0]
            return render_template("session.html",name = name,datas = result)
        else:
            return redirect("/")
    else:
        flash("Mauvais token")
        return redirect("/")

@app.route("/in-session",methods = ["POST"])
def in_session():
    req = request.get_json()
    users = log.users_in_sess(req["token"])
    for x in users:
        if x[0] == req["name"]:
            if x[1] == False:
                res = make_response(jsonify({"message": "OK","users":users,"voteur":req["name"]}), 200)
                return res
            else:
                res = make_response(jsonify({"message":"Vous avez déjà voté."}),200)
                return res
    
    res = make_response(jsonify({"message": "Nom invalide"}), 200)
    return res

@app.route("/new-session")
def nouvelle():
    if "user" in session:
        return render_template("nouvelle.html",name = session["user"])
    else:
        return redirect("/login")

@app.route("/vote",methods = ["POST"])
def vote():
    req = request.get_json()
    name = req["name"]
    voted = req["voted"]
    token = req["token"]
    log.add_voted(name = name,voted = voted,token =token)
    res = make_response(jsonify({"message": "A voter !"}), 200)
    return res

@app.route("/login",methods = ["GET","POST"])
def login():
    if request.method == "GET":
        if "user" in session:
            return redirect("/compte")
        else:
            return render_template("login.html")
    else:
        if "user" in session:
            return redirect("/logout")
        
        else:
            req = request.get_json()
            name = req["name"]
            password = req["passw"]
            result = log.trier_user(name,password)
            if result:
                session["user"] = name
                res = make_response(jsonify({"message":"OK"}),200)
                return res
            
            res = make_response(jsonify({"message":"nope"}),200)
            return res

@app.route("/project")
def project():
    return render_template("prog.html")


@app.route("/change-elige", methods = ["POST"])
def change_eligible():
    if "user" in session:
        req = request.get_json()
        nom = req["name"]
        token = req["token"]
        elige = req["elig"]
        if log.proprio(session["user"],token):
            if elige.lower() == "oui" or elige.lower() == "non":
                log.change_elec(nom,token,elige)
                res = make_response(jsonify({"message":"ok"}),200)
                return res
            else:
                res = make_response(jsonify({"message":"nope"}),200)
                return res
        else:
            return redirect("/logout")
    else:
        return redirect('/login')

@app.route("/supprim_sess",methods=["POST"])
def supp_sess():
    if "user" in session:
        req = request.get_json()
        print(req)
        token = req["token"]
        if log.proprio(session["user"],token):
            log.delete_sess(token)
            res = make_response(jsonify({"message":"dell"}),200)
            return res
        else:
            print("Not proprio")
            res = make_response(jsonify({"message":"nope"}),200)
            return res
    else:
        print("No session")
        res = make_response(jsonify({"message":"nope"}),200)
        return res

@app.route("/sessions")
def view_sess():
    if "user" in session:
        sessions = log.get_sessions(session["user"])[0][0]
        infos = []
        voted = []
        for x in sessions:
            try:
                temp = []      
                temp.append(log.get_sess_name(x)[0])
                temp.append(log.users_in_sess(x))
                infos.append(temp)
                voted.append(log.get_voted(x))
            except:
                pass
        
        return render_template("sessions_infos.html",result = infos, len = len,voted = voted)
    else:
        return redirect("/login")

@app.route("/createacc",methods = ["POST"])
def create_account():
    req = request.get_json()

    name = req["name"]
    email = req["email"]
    passw = req["password"]
    log.create_account(name,email,passw)
    res = make_response(jsonify({"message":"OK"}),200)
    session["user"] = email
    return res

@app.route("/create-sess",methods = ["POST"])
def create_session():
    req = request.get_json()
    token = log.create_sess(req["titre"],req["participants"],session["user"])
    res = make_response(jsonify({"message":"OK","token":token}),200)
    return res

@app.route("/create")
def create():
    return render_template("create.html")

@app.route("/compte")
def compte():
    if "user" in session:
        result = log.get_usr_name(session["user"])
        return render_template("account.html",name = result[1],email = session["user"])
    else:
        return redirect("/login")

@app.route("/logout")
def logout():
    session.pop("user",None)
    return redirect("/")

if __name__ == "__main__":
    app.run(debug=True,port = 5700)