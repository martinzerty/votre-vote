function get_name() {
    document.getElementById("monret").classList.add("message")
    document.getElementById("monret").innerHTML = `Chargement <svg fill="black" class="tourne" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M8 2.5a5.487 5.487 0 00-4.131 1.869l1.204 1.204A.25.25 0 014.896 6H1.25A.25.25 0 011 5.75V2.104a.25.25 0 01.427-.177l1.38 1.38A7.001 7.001 0 0114.95 7.16a.75.75 0 11-1.49.178A5.501 5.501 0 008 2.5zM1.705 8.005a.75.75 0 01.834.656 5.501 5.501 0 009.592 2.97l-1.204-1.204a.25.25 0 01.177-.427h3.646a.25.25 0 01.25.25v3.646a.25.25 0 01-.427.177l-1.38-1.38A7.001 7.001 0 011.05 8.84a.75.75 0 01.656-.834z"></path></svg>`
    var name = document.getElementById("name");
    var token = document.getElementById("infocode");
    var sess_name = document.getElementById("infoname");


    var entry = {
        name: name.value,
        token: token.textContent,
        sess_name: sess_name.textContent,
    };

    fetch(`${window.origin}/in-session`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(entry),
        cache: "no-cache",
        headers: new Headers({
            "content-type": "application/json"
        })
    })
    .then(response => response.json())
    .then(data => {
        document.getElementById("monret").innerHTML = data['message']
        if(data["message"] == "OK"){
            document.getElementById("name-voteur").innerHTML = `Bonjour, ${name.value}`
            window.nom = name.value
            place_form(data["users"])
        }

    })
    .catch(function(error) {
            console.log("Fetch error: " + error);
            document.getElementById("monret").innerHTML = data['message']
        });
}

function place_form(datas){
    var to_insert = ''
    document.getElementById("voting-place").style.display = 'block'
    for(let step =0; step < datas.length; step++){
        const temp = datas[step]
        if(temp[3] == true){
            to_insert = to_insert + `<div class="radiobtn"><input type="radio" required class="demo" id="candidat${step}" name="drone" value="${datas[step][0]}"/><label for="candidat${step}">${datas[step][0]}</label></div>`
        }
    }
    to_insert = to_insert + `<button class="btn submit" onclick="voter()" >Voter <svg fill="white" style="vertical-align:-2px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="16" height="16"><path fill-rule="evenodd" d="M1.75 3A1.75 1.75 0 000 4.75v14c0 .966.784 1.75 1.75 1.75h20.5A1.75 1.75 0 0024 18.75v-14A1.75 1.75 0 0022.25 3H1.75zM1.5 4.75a.25.25 0 01.25-.25h20.5a.25.25 0 01.25.25v.852l-10.36 7a.25.25 0 01-.28 0l-10.36-7V4.75zm0 2.662V18.75c0 .138.112.25.25.25h20.5a.25.25 0 00.25-.25V7.412l-9.52 6.433c-.592.4-1.368.4-1.96 0L1.5 7.412z"></path></svg></button`
    document.getElementById("enter-name").style.display = "None"
    document.getElementById('form-candidats').innerHTML = to_insert;
}

function voter(){
    var voters = document.getElementsByName("drone")
    for(i=0; i < voters.length; i++){
        if (voters[i].checked){
            var voted = voters[i].value
        }
    }
    var token = document.getElementById("infocode");
    var sess_name = document.getElementById("infoname");

    if (confirm(`Sûr de vouloir voter pour ${voted} ?`)){
        var entry = {
        voted: voted,
        name: window.nom,
        token: token.textContent
        };

        fetch(`${window.origin}/vote`, {
            method: "POST",
            credentials: "include",
            body: JSON.stringify(entry),
            cache: "no-cache",
            headers: new Headers({
                "content-type": "application/json"
            })
        })
        .then(response => response.json())
        .then(data => {
            document.getElementById("monret").className = ""
            document.getElementById("monret").classList.add("success")
            document.getElementById("monret").innerHTML = data['message']
            if(data["message"] == "A voter !"){
                document.getElementById("voting-place").style.display = 'none'
                document.getElementById("remerci").style.display = 'block'
                document.getElementById("retour").style.display = 'block'
            }

        })
        .catch(function(error) {
                console.log("Fetch error: " + error);
        });
    }   
}