var coef = 0

function add_people(){
    window.coef = document.getElementById("pers").value
    document.getElementById("intro").style.display = 'none'
    document.getElementById("step").innerHTML = 'Deuxième étape'
    for(x = 1; x <= window.coef; x++){
        document.getElementById("people").innerHTML = document.getElementById("people").innerHTML + `<div class='inputs'><input type='text' class='into' placeholder='Ex: Jacky' required name='pers${x}' id="pers${x}"><label for='elige'> Éligile : </label> <input type='checkbox' name='eligile${x}' id='eligible${x}'></div>`
    }
    document.getElementById("people").innerHTML += "<button onclick='change_nb()' id='change_btn' class='btn'>Changer</button>"
    document.getElementById("people").style.display = 'block'
    document.getElementById('nb-particip').style.display = 'none'
    document.getElementById('final').style.display = 'block'
}

function go(){
        var title = document.getElementById("titre").value
        var personnes = new Map()
        for(x = 1; x <= coef; x++){
            var temp_pers = document.getElementById(`pers${x}`).value
            var temp = document.getElementById(`eligible${x}`)
            if(temp.checked){
                personnes.set(temp_pers,"Y")
            }
            else{
                personnes.set(temp_pers,"N")
            }
        }

        var entry = {
            participants: Object.fromEntries(personnes),
            titre: title
        }
        document.getElementById("monret").innerHTML = "Chargement..."
        fetch(`${window.origin}/create-sess`, {
            method: "POST",
            credentials: "include",
            body: JSON.stringify(entry),
            cache: "no-cache",
            headers: new Headers({
                "content-type": "application/json"
            })
        })
        .then(response => response.json())
        .then(data => {
            document.getElementById("monret").className = ""
            document.getElementById("monret").classList.add("success")
            if(data["message"] == "OK"){
                document.getElementById("monret").innerHTML = "Tout est bon !"
                document.getElementById("comp").innerHTML = `<h3 class="merci">Bravo, vous avez créé votre session !</h3><p>Vous avez créé votre session de vote. En voici le token <b id="token">${data["token"]}</b><br><a href="session/${data["token"]}">Y aller</a>`
                document.getElementById("comp").style.display = 'block'
                document.getElementById("formulaire").style.display = 'none'
            }
            else{
                console.log(data['message'])
            }

        })
        // .catch(function(error) {
        //         console.log("Fetch error: " + error);
        // });
    }

function change_nb(){
    window.coef = 0
    document.getElementById("intro").style.display = 'block'
    document.getElementById('nb-particip').style.display = 'block'
    document.getElementById("people").style.display = 'none'
    document.getElementById('final').style.display = 'none'
    var inputs = document.getElementsByClassName('inputs')
    console.log("Inputs = " + inputs.length)
    for(i=0; i < inputs.length; i++ ){
        console.log(i)
        console.log(inputs[i])
        inputs[i].remove()
    }
    document.getElementById("change_btn").remove()
}