var modal = document.getElementById("myModal");

var span = document.getElementsByClassName("close")[0];

function modif(x,y){
    console.log(x,y)
    console.log(document.getElementById(`nom${x}&${y}`))
    document.getElementById("form-modif").innerHTML = `<h3>Changer l'éligibilité de ${document.getElementById(`nom${x}&${y}`).textContent} </h3><input class="into" id="elige${x}&${y}" type="text" placeholder="tapez Oui ou Non"/></label><button class="submit" onclick=valid("${document.getElementById(`nom${x}&${y}`).textContent}",${x},${y}) >Valider</button>`
    modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

function valid(nom, x , y){
    var token = document.getElementById(`infocode${x}`).textContent
    console.log("On change ",nom," de la session ", token," eligible ? : ",document.getElementById(`elige${x}&${y}`).value)
    var entry = {
        name: nom,
        token: token,
        elig: document.getElementById(`elige${x}&${y}`).value,
    };

    fetch(`${window.origin}/change-elige`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(entry),
        cache: "no-cache",
        headers: new Headers({
            "content-type": "application/json"
        })
    })
    .then(response => response.json())
    .then(data => {
        if (data['message'] == 'ok'){
            if (entry.elig.toLowerCase() == 'oui'){
                document.getElementById(`etat${x}&${y}`).innerHTML = `Oui <svg xmlns="http://www.w3.org/2000/svg" fill="green" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path></svg>`
            }
            else{
                document.getElementById(`etat${x}&${y}`).innerHTML = `Non <svg xmlns="http://www.w3.org/2000/svg"  fill="red" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path></svg>`
            }
        }
        modal.style.display = "none";
    })
    .catch(function(error) {
            console.log("Fetch error: " + error);
            document.getElementById("monret").innerHTML = data['message']
        })

}

function supp(x){
    token = document.getElementById(`infocode${x}`).textContent
    if (confirm(`Sûr(e) de vouloir supprimer la session ${document.getElementById(`infoname${x}`).textContent}`)){
        var entry = {
        token : token
        }
        fetch(`${window.origin}/supprim_sess`, {
            method: "POST",
            credentials: "include",
            body: JSON.stringify(entry),
            cache: "no-cache",
            headers: new Headers({
                "content-type": "application/json"
            })
        })
        .then(response => response.json())
        .then(data => {
            if (data['message'] == 'dell'){
                document.getElementById(`table${x}`).style.display = 'none'
            }
            else{
                document.location.href="/"
            }
            modal.style.display = "none";
        })
        // .catch(function(error) {
        //         console.log("Fetch error: " + error);
        //         document.getElementById("monret").innerHTML = data['message']
        //     })
}
}


window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}