from flask import Flask, render_template, flash, redirect, request, make_response, jsonify, session, send_from_directory
import datalogger
from datetime import timedelta
import os

app = Flask(__name__)
app.secret_key = os.environ["SECRET_KEY"]
app.permanent_session_lifetime = timedelta(minutes=10)
log = datalogger.Logger()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/session/<token>")
def acces_sess(token):
    result = [1]
    if len(result) > 0:
        return render_template("session.html",name = [0,"token&d3m0","Session Démo"],datas = ["Jean"])
    else:
        return redirect("/")

@app.route("/in-session",methods = ["POST"])
def in_session():
    req = request.get_json()
    users = [["Jean",False,'',True],["Louise",False,'',True]]
    for x in users:
        print(x)
        print(req)
        if x[0] == req["name"]:
            if x[1] == False:
                res = make_response(jsonify({"message": "OK","users":users,"voteur":req["name"]}), 200)
                return res
            else:
                res = make_response(jsonify({"message":"Vous avez déjà voté."}),200)
                return res
    
    res = make_response(jsonify({"message": "Nom invalide"}), 200)
    return res

@app.route("/new-session")
def nouvelle():
    if "user" in session:
        return render_template("nouvelle.html",name = session["user"])
    else:
        return redirect("/login")

@app.route("/vote",methods = ["POST"])
def vote():
    req = request.get_json()
    name = req["name"]
    voted = req["voted"]
    token = req["token"]
    res = make_response(jsonify({"message": "A voter !"}), 200)
    return res

@app.route("/login",methods = ["GET","POST"])
def login():
    if request.method == "GET":
        if "user" in session:
            return redirect("/compte")
        else:
            return render_template("login.html")
    else:
        if "user" in session:
            return redirect("/logout")
        
        else:
            req = request.get_json()
            name = req["name"]
            password = req["passw"]
            result = True
            if result:
                session["user"] = name
                res = make_response(jsonify({"message":"OK"}),200)
                return res
            
            res = make_response(jsonify({"message":"nope"}),200)
            return res

@app.route("/project")
def project():
    return render_template("prog.html")

@app.route("/sessions")
def view_sess():
    if "user" in session:
        return "ALLOwed"
    else:
        return redirect("/login")

@app.route("/createacc",methods = ["POST"])
def create_account():
    req = request.get_json()

    name = req["name"]
    email = req["email"]
    passw = req["password"]
    res = make_response(jsonify({"message":"OK"}),200)
    session["user"] = email
    return res

@app.route("/create-sess",methods = ["POST"])
def create_session():
    req = request.get_json()
    token = "3nc0r3_d3m0"
    res = make_response(jsonify({"message":"OK","token":token}),200)
    return res

@app.route("/create")
def create():
    return render_template("create.html")

@app.route("/compte")
def compte():
    if "user" in session:
        result = "démo-usr"
        return render_template("account.html",name = result[1],email = session["user"])
    else:
        return redirect("/login")

@app.route("/logout")
def logout():
    session.pop("user",None)
    return redirect("/")

if __name__ == "__main__":
    app.run(debug=True,port = 5000)