from cryptography.fernet import Fernet
import psycopg2
import configparser
import random
import string

class Logger(object):
    def __init__(self):
        with open("ariana.key","rb") as file:
            self.key = file.read()
    
    def reset(self,password):
        if password == "251378197345756792":
            fer = Fernet(self.key)
            config = configparser.ConfigParser()
            config["DATAS"] = {
                "user": fer.encrypt(b"epeslgftqdprfo"),
                "dbname": fer.encrypt(b"d8f5g7aqcaf8g9"),
                "host": fer.encrypt(b"ec2-34-247-151-118.eu-west-1.compute.amazonaws.com"),
                "password": fer.encrypt(b"4074dd00c7d3b4d510c4e1572a8a253e3c69b177789530f773ddccca995061bd")
            }
            with open("henri.ini","w") as configfile:
                config.write(configfile)
        else:
            pass

    def get_config(self):
        fer = Fernet(self.key)
        print(self.key)
        config = configparser.ConfigParser()
        config.read('henri.ini')
        dico = {}
        print(bytes(config["DATAS"]["user"],"utf-8"))
        dico["user"] = str(fer.decrypt(bytes(config["DATAS"]["user"],"utf-8")))[2:-1]
        dico["dbname"] = str(fer.decrypt(bytes(config["DATAS"]["dbname"],"utf-8")))[2:-1]
        dico["host"] = str(fer.decrypt(bytes(config["DATAS"]["host"],"utf-8")))[2:-1]
        dico["password"] = str(fer.decrypt(bytes(config["DATAS"]["password"],"utf-8")))[2:-1]
        return dico

    def users_in_sess(self,token):
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SELECT * FROM sess_usr WHERE sess_token = '{token}'")
        
        ret = cur.fetchall()
        con.close()
        return ret

    def create_sess(self,name,users, poster):
        """
        users in a list of dict of names. Ex: « [{
            "name":"Jean",
            "eligible":True
        },{
            "name":"Jeannette",
            "eligible":False
        }] »
        """
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])

        possible_char = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!%*+-@"
        token = ""
        for x in range(30):
            token = token + random.choice(possible_char)

        cur = con.cursor()
        cur.execute(f"INSERT INTO session (token,name) VALUES('{token}','{name}')")
        con.commit()
        for user in users:
            if users[user] == "Y":
                cur.execute(f"""INSERT INTO sess_usr VALUES('{user}',false,'{token}',true)""")
            else:
                cur.execute(f"""INSERT INTO sess_usr VALUES('{user}',false,'{token}',false)""")
            con.commit()

        cur.execute(f"UPDATE users SET sess = array_append(sess,'{token}') WHERE email = '{poster}' ")
        con.commit()
        con.close()
        return token
    
    def get_sess_name(self,token):
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SElECT * FROM session WHERE token = '{token}'")
        result = cur.fetchall()
        con.close()
        return result
    
    def add_voted(self,name,voted,token):
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()   
        for x in self.users_in_sess(token):
            if x[0] == name:
                if x[1] == False:
                    cur.execute(f"INSERT INTO voted VALUES('{voted}','{token}')")
                    con.commit()
                    cur.execute(f"UPDATE sess_usr SET voted = true WHERE name = '{name}'")  
                    con.commit()
                    con.close()  
                    return True
                else:
                    return "Déjà voté"
        
        return "Unknown name"
    
    def trier_user(self,name,password):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute("SELECT * FROM users")
        rows = cur.fetchall()
        for r in rows:
            if r[2] == name and password == str(fer.decrypt(bytes(r[3],"utf-8")))[2:-1]:
                return True

        return False
    
    def create_account(self,name,email,password):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"INSERT INTO users (name,email,password) VALUES('{name}','{email}','{str( fer.encrypt( bytes(password,'utf-8') ) )[2:-1]}')")
        con.commit()
        con.close()

        return True
    
    def get_usr_name(self,email):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SELECT * FROM users WHERE email = '{email}'")
        rows = cur.fetchall()
        to_send = []
        for r in rows:
            for x in range(len(r)):
                to_send.append(r[x])
        to_send[3] = str(fer.decrypt(bytes(rows[0][3],'utf-8')))[2:-1]
        con.close()
        return to_send
    
    def get_sessions(self,email):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SELECT sess FROM users WHERE email = '{email}'")
        rows = cur.fetchall()
        con.close()
        return rows

    def change_elec(self,name,token,elig):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()

        if elig == "oui":
            elig = True
        else:
            elig = False

        cur.execute(f"UPDATE sess_usr SET eligible = {elig} WHERE name = '{name}' AND sess_token = '{token}'")
        con.commit()
        con.close()
        return True

    def proprio(self,user,token):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SELECT sess FROM users WHERE email = '{user}'")
        rows = cur.fetchall()
        con.close()
        print(rows)
        for x in rows[0]:
            for y in x:
                if token == y:
                    return True
        
        return False

    def delete_sess(self,token):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"DELETE FROM sess_usr WHERE sess_token = '{token}'")
        con.commit()
        cur.execute(f"DELETE FROM session WHERE token = '{token}'")
        con.commit()
        con.close()
        return True

    def get_voted(self,token):
        fer = Fernet(self.key)
        configuration = self.get_config()
        con = psycopg2.connect(dbname = configuration["dbname"], user = configuration["user"],
                                host = configuration["host"], password = configuration["password"])
        cur = con.cursor()
        cur.execute(f"SELECT * FROM voted WHERE token = '{token}'")
        rows = cur.fetchall()
        dico = {}
        for r in rows:
            if str(r[0]) in dico:
                dico[r[0]] += 1
            else:
                dico[r[0]] = 1
        con.close()

        return dico        